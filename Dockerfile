FROM openjdk:8

COPY target/person-register.jar /

CMD java -jar -Dspring.profiles.active=docker person-register.jar
