
# People Register

SPBSTU Project

To deploy:

1) Install Git, Docker, Maven 
2) Check out project with 'git clone';
3) Build project with 'mvn clean install'
4) Create image with 'docker-compose build'
5) Run container with 'docker-compose up'
6) Go to https://localhost:8443

To test:

1) Install Git, Docker, Maven 
2) Check out project with 'git clone';
3) Create database with 'docker run -p 5432:5432 postgres:10'
4) Run the tests from Intellij IDEA