package application.document.dtos;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class DocumentDto {

        private Long id;

        private String country;

        private String documentNumber;

        private String type;

        private Long personId;

        protected LocalDate startDate;

        protected LocalDate expiredDate;

}
