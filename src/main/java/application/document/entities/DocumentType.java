package application.document.entities;

import java.util.Arrays;
import java.util.List;

public class DocumentType {

    public static final String PASSPORT = "PASSPORT";
    public static final String INN = "INN";
    public static final String SNEELS = "SNEELS";

    public static final List<String> documentTypes = Arrays.asList(PASSPORT,INN,SNEELS);
}
