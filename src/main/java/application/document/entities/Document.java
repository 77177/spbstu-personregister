package application.document.entities;

import application.person.entities.Person;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String country;

    private String documentNumber;

    private String type;

    @ManyToOne
    @JsonBackReference
    private Person person;

    protected LocalDate startDate;

    protected LocalDate expiredDate;

    public void apply(Document document){
        this.country = document.getCountry();
        this.documentNumber = document.getDocumentNumber();
        this.type = document.getType();
        this.person = document.getPerson();
        this.startDate = document.getStartDate();
        this.expiredDate = document.getExpiredDate();
    }
}
