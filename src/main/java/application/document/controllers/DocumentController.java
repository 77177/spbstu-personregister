package application.document.controllers;

import application.document.dtos.DocumentDto;
import application.document.entities.Document;
import application.document.services.DocumentService;
import application.template.controller.ControllerActions;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/documents")
public class DocumentController implements ControllerActions<Document, DocumentDto> {

    private final DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @Override
    @GetMapping("/get/{id}")
    public Document get(@PathVariable long id) {
        return documentService.get(id);
    }

    @Override
    @PostMapping("/post")
    public Long post(@RequestBody DocumentDto dto) {
        return documentService.createOrUpdate(dto);
    }

    @Override
    @PutMapping("/put")
    public Long put(@RequestBody DocumentDto dto) {
        return documentService.createOrUpdate(dto);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(long id) {
        documentService.delete(id);
    }
}
