package application.document.repositories;

import application.document.entities.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface DocumentRepository extends JpaRepository<Document, Long> {

    @Query(value = "select *\n" +
            "from document\n" +
            "order by \n" +
            "(1 - (conv_dn(document_number) <-> :documentNumber))\n" +
            "desc\n" +
            "limit 5;",
            nativeQuery = true
    )
    List<Document> fuzzyFind(@Param("documentNumber") String documentNumber);

    @Query(value = "select \n" +
            "(1 - (conv_dn(document_number) <-> :documentNumber))\n" +
            "from document\n" +
            "order by \n" +
            "(1 - (conv_dn(document_number) <-> :documentNumber))\n" +
            "desc\n" +
            "limit 5;",
            nativeQuery = true
    )
    List<Double> getMetric(@Param("documentNumber") String documentNumber);
}
