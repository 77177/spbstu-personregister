package application.document.services;

import application.document.dtos.DocumentDto;
import application.document.entities.Document;
import application.document.entities.DocumentType;
import application.document.repositories.DocumentRepository;
import application.person.entities.Person;
import application.template.service.ServiceActions;
import application.utility.Converter;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class DocumentService implements ServiceActions<Document, DocumentDto> {

    private final DocumentRepository documentRepository;

    final Converter converter;

    public DocumentService(DocumentRepository documentRepository, Converter converter) {
        this.documentRepository = documentRepository;
        this.converter = converter;
    }

    @Override
    @Transactional
    public Document get(long id) {
        return documentRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("This document doesn't exist, id - " + id));
    }

    public boolean hasProperInfo(DocumentDto dto) {

        String documentType = dto.getType();

        boolean isProper = true;

        if (documentType.equals(DocumentType.PASSPORT)) {
            if (dto.getDocumentNumber().split(";").length != 2) {
                isProper = false;
            }
        } else if (documentType.equals(DocumentType.INN) || documentType.equals(DocumentType.SNEELS)) {
            if (dto.getDocumentNumber().split(";").length != 1) {
                isProper = false;
            }
        } else {
            isProper = false;
        }

        return isProper;
    }

    @Override
    @Transactional
    public Long createOrUpdate(DocumentDto dto) {

        if(!hasProperInfo(dto)){
            throw new IllegalArgumentException("faulty DTO");
        };

        Long documentId = dto.getId();

        Document document = converter.documentDtoToDocument(dto);

        if (documentRepository.existsById(documentId)) {
            Document documentFromRepo = documentRepository.getOne(documentId);
            documentFromRepo.apply(document);
        } else {
            Document savedDocument = documentRepository.save(document);
            documentId = savedDocument.getId();
        }

        return documentId;

    }

    @Override
    @Transactional
    public void delete(long id) {
        documentRepository.deleteById(id);
    }

    @Transactional
    public List<Document> getAllDocuments() {
        return documentRepository.findAll();
    }

    @Transactional
    public void deleteAllDocuments() {
        documentRepository.deleteAll();
    }

    @Transactional
    public List<Person> findPersonByDocNumberNoMetric(String documentNumber) {

        List<Document> documents = documentRepository.fuzzyFind(documentNumber);

        return documents.stream()
                .map(Document::getPerson)
                .collect(toList());
    }

    @Transactional
    public List<Pair<Double, Person>> findPersonByDocNumber(String documentNumber) {

        List<Document> documents = documentRepository.fuzzyFind(documentNumber);

        List<Person> people = documents.stream()
                .map(Document::getPerson)
                .collect(toList());

        List<Double> metric = documentRepository.getMetric(documentNumber);

        List<Pair<Double, Person>> pairs = new ArrayList<>();

        for (int i = 0; i < documents.size(); i++) {
            pairs.add(Pair.of(metric.get(i), people.get(i)));
        }

        return pairs;
    }
}
