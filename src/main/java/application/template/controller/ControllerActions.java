package application.template.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

public interface ControllerActions <T,DT> {

    T get(@PathVariable long id);

    Long post(@RequestBody DT dto);

    Long put(@RequestBody DT dto);

    void delete(@PathVariable long id);

}
