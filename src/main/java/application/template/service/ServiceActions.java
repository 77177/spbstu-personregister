package application.template.service;

import javax.transaction.Transactional;

public interface ServiceActions <T, DT> {

    T get(long id);

    @Transactional
    Long createOrUpdate(DT dto);

    @Transactional
    void delete(long id);

}
