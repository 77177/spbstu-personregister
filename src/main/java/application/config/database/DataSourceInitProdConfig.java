package application.config.database;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;

import static application.utility.Constants.DEFAULT;
import static application.utility.Constants.DOCKER;

@Configuration
@Profile(value = {DOCKER})
public class DataSourceInitProdConfig {

    @Bean
    public DataSourceInitializer dataSourceInitializer(final DataSource dataSource) {
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();
        DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
        resourceDatabasePopulator.addScript(new ClassPathResource("/postgresql/schema/schemaPostgres.sql"));
        resourceDatabasePopulator.addScript(new ClassPathResource("/postgresql/schema/auditPostgres.sql"));
        dataSourceInitializer.setDataSource(dataSource);
        dataSourceInitializer.setDatabasePopulator(resourceDatabasePopulator);
        return dataSourceInitializer;
    }
}
