package application.person.repositories;

import application.person.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query(value = "select *\n" +
            "from person\n" +
            "order by (\n" +
            "cast(upper(email) like upper('%'|| :email ||'%') as int) +\n" +
            "cast(birthday = :birthday as int) +\n" +
            "cast(upper(telephone) like upper('%'|| :telephone ||'%') as int) +\n" +
            "(1 - (conv_f(f) <-> :f )) +\n" +
            "(1 - (conv_i(i) <-> :i )) +\n" +
            "(1 - (conv_f(o) <-> :o ))) \n" +
            "desc\n" +
            "limit 5;",
            nativeQuery = true
    )
    List<Person> fuzzyFind(@Param("email") String email,
                           @Param("f") String f,
                           @Param("i") String i,
                           @Param("o") String o,
                           @Param("birthday") LocalDate birthday,
                           @Param("telephone") String telephone);

    @Query(value = "select (" +
            "cast(upper(email) like upper('%'|| :email ||'%') as int) +\n" +
            "cast(birthday = :birthday as int) +\n" +
            "cast(upper(telephone) like upper('%'|| :telephone ||'%') as int) +\n" +
            "(1 - (conv_f(f) <-> :f )) +\n" +
            "(1 - (conv_i(i) <-> :i )) +\n" +
            "(1 - (conv_f(o) <-> :o ))) \n" +
            "from person\n" +
            "order by (\n" +
            "cast(upper(email) like upper('%'|| :email ||'%') as int) +\n" +
            "cast(birthday = :birthday as int) +\n" +
            "cast(upper(telephone) like upper('%'|| :telephone ||'%') as int) +\n" +
            "(1 - (conv_f(f) <-> :f )) +\n" +
            "(1 - (conv_i(i) <-> :i )) +\n" +
            "(1 - (conv_f(o) <-> :o ))) \n" +
            "desc\n" +
            "limit 5;",
            nativeQuery = true
    )
    List<Double> getMetric(@Param("email") String email,
                           @Param("f") String f,
                           @Param("i") String i,
                           @Param("o") String o,
                           @Param("birthday") LocalDate birthday,
                           @Param("telephone") String telephone);

}
