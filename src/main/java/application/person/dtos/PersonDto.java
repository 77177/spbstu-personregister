package application.person.dtos;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class PersonDto {

    private LocalDate birthday;
    private Long id;
    private String f;
    private String i;
    private String o;
    private String email;
    private String telephone;
}

