package application.person.controllers;

import application.person.dtos.PersonDto;
import application.person.entities.Person;
import application.person.services.PersonService;
import application.template.controller.ControllerActions;
import org.springframework.data.util.Pair;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/persons")
public class PersonController implements ControllerActions<Person, PersonDto> {

    private final PersonService personService;

    public PersonController(PersonService PersonService) {
        this.personService = PersonService;
    }

    @GetMapping("/get/{id}")
    public Person get(@PathVariable long id) {
        return personService.get(id);
    }

    @PostMapping("/post")
    public Long post(@RequestBody PersonDto personDto) {
        return personService.createOrUpdate(personDto);
    }

    @PutMapping("/put")
    public Long put(@RequestBody PersonDto personDto) {
        return personService.createOrUpdate(personDto);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id) {
        personService.delete(id);
    }

    @GetMapping("/get/all")
    public List<Person> getAllPersons() {
        return personService.getAllPersons();
    }

    @GetMapping("/find")
    public List<Pair<Double, Person>> findPerson(@RequestParam(required = false) String email,
                                                 @RequestParam(required = false) String f,
                                                 @RequestParam(required = false) String i,
                                                 @RequestParam(required = false) String o,
                                                 @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate birthday,
                                                 @RequestParam(required = false) String telephone) {
        return personService.findPerson(email, f, i, o, telephone, birthday);
    }

    @GetMapping("/findNoMetric")
    public List<Person> findPersonNoMetric(@RequestParam(required = false) String email,
                                           @RequestParam(required = false) String f,
                                           @RequestParam(required = false) String i,
                                           @RequestParam(required = false) String o,
                                           @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate birthday,
                                           @RequestParam(required = false) String telephone) {
        return personService.findPersonNoMetric(email, f, i, o, telephone, birthday);
    }

    @GetMapping("/findByDoc")
    public List<Pair<Double, Person>> findPersonByDocumentNumber(@RequestParam(required = false) String documentNumber) {
        return personService.findPersonByDocumentNumber(documentNumber);
    }

    @GetMapping("/findByDocNoMetric")
    public List<Person> findPersonByDocumentNumberNoMetric(@RequestParam(required = false) String documentNumber) {
        return personService.findPersonByDocumentNumberNoMetric(documentNumber);
    }
}