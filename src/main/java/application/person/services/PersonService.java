package application.person.services;

import application.document.services.DocumentService;
import application.person.dtos.PersonDto;
import application.person.entities.Person;
import application.person.repositories.PersonRepository;
import application.template.service.ServiceActions;
import application.utility.Converter;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
public class PersonService implements ServiceActions<Person, PersonDto> {

    private final PersonRepository personRepository;

    private final Converter converter;

    private final DocumentService documentService;

    public PersonService(PersonRepository personRepository, Converter converter, DocumentService documentService) {
        this.personRepository = personRepository;
        this.converter = converter;
        this.documentService = documentService;
    }

    @Transactional
    public Person get(long id) {
        Optional<Person> personOptional = personRepository.findById(id);
        return personOptional.orElseThrow(() -> new IllegalArgumentException("This person doesn't exist, id - " + id));
    }

    @Transactional
    public Long createOrUpdate(PersonDto personDto) {

        Long personId = personDto.getId();

        if (personRepository.existsById(personId)) {
            Person person = converter.personDtoToPerson(personDto);
            Person personFromRepo = personRepository.getOne(personId);
            personFromRepo.apply(person);
        } else {
            Person person = converter.personDtoToPersonDocEmpty(personDto);
            Person savedPerson = personRepository.save(person);
            personId = savedPerson.getId();
        }
        return personId;
    }

    @Transactional
    public void delete(long id) {
        personRepository.deleteById(id);
    }

    @Transactional
    public List<Person> getAllPersons() {
        return personRepository.findAll();
    }

    @Transactional
    public void deleteAllPersons() {
        personRepository.deleteAll();
    }

    private String ifNull(String value) {
        if (value == null) {
            return "";
        }
        return value;
    }

    private Long ifNull(Long value) {
        if (value == null) {
            return -1L;
        }
        return value;
    }

    private LocalDate ifNull(LocalDate value) {
        if (value == null) {
            return LocalDate.of(1, 1, 1);
        }
        return value;
    }


    @Transactional
    public List<Pair<Double, Person>> findPerson(String email, String f, String i, String o, String telephone, LocalDate birthday) {

        email = ifNull(email);
        f = ifNull(f);
        i = ifNull(i);
        o = ifNull(o);
        telephone = ifNull(telephone);
        birthday = ifNull(birthday);


        List<Person> personSearchList = personRepository.fuzzyFind(email, f, i, o, birthday, telephone);

        List<Double> metrics = personRepository.getMetric(email, f, i, o, birthday, telephone).stream()
                .map(d -> (d/6 *100))
                .collect(toList());

        List<Pair<Double, Person>> pairs = new ArrayList<>();

        for (int j = 0; j < personSearchList.size(); j++) {
            pairs.add(Pair.of(metrics.get(j), personSearchList.get(j)));
        }


        return pairs;
    }

    @Transactional
    public List<Person> findPersonNoMetric(String email, String f, String i, String o, String telephone, LocalDate birthday) {

        email = ifNull(email);
        f = ifNull(f);
        i = ifNull(i);
        o = ifNull(o);
        telephone = ifNull(telephone);
        birthday = ifNull(birthday);

        List<Person> personList = personRepository.fuzzyFind(email, f, i, o, birthday, telephone);

        return personList;
    }

    public List<Pair<Double, Person>> findPersonByDocumentNumber(String documentNumber) {

        documentNumber = ifNull(documentNumber);

        List<Pair<Double, Person>> personByDocNumber = documentService.findPersonByDocNumber(documentNumber);

        personByDocNumber = personByDocNumber.stream()
                .map(pair -> Pair.of((pair.getFirst() / 1 * 100), pair.getSecond()))
                .collect(toList());

        return personByDocNumber;
    }

    public List<Person> findPersonByDocumentNumberNoMetric(String documentNumber) {

        documentNumber = ifNull(documentNumber);

        return documentService.findPersonByDocNumberNoMetric(documentNumber);
    }
}
