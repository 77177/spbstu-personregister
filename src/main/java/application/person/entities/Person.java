package application.person.entities;

import application.document.entities.Document;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate birthday;
    private String f;
    private String i;
    private String o;
    private String email;
    private String telephone;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Document> documents;

    public void apply(Person person){
        this.f = person.getF();
        this.i = person.getI();
        this.o = person.getO();
        this.email = person.getEmail();
        this.telephone = person.getTelephone();
        this.birthday = person.getBirthday();
        this.documents = person.getDocuments();
    }
}
