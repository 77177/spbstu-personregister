package application.utility;

import application.document.dtos.DocumentDto;
import application.document.entities.Document;
import application.person.dtos.PersonDto;
import application.person.entities.Person;
import application.person.services.PersonService;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class Converter {

    private final PersonService personService;

    public Converter(@Lazy PersonService personService) {
        this.personService = personService;
    }

    public Person personDtoToPerson(PersonDto personDto) {

        return Person.builder()
                .id(personDto.getId())
                .birthday(personDto.getBirthday())
                .f(personDto.getF())
                .i(personDto.getI())
                .o(personDto.getO())
                .email(personDto.getEmail())
                .telephone(personDto.getTelephone())
                .documents(personService.get(personDto.getId()).getDocuments())
                .build();
    }

    public Person personDtoToPersonDocEmpty(PersonDto personDto) {

        return Person.builder()
                .id(personDto.getId())
                .birthday(personDto.getBirthday())
                .f(personDto.getF())
                .i(personDto.getI())
                .o(personDto.getO())
                .email(personDto.getEmail())
                .telephone(personDto.getTelephone())
                .documents(Collections.emptyList())
                .build();
    }

    public Document documentDtoToDocument(DocumentDto dto) {

        return Document.builder()
                .id(dto.getId())
                .person(personService.get(dto.getPersonId()))
                .startDate(dto.getStartDate())
                .expiredDate(dto.getExpiredDate())
                .documentNumber(dto.getDocumentNumber())
                .country(dto.getCountry())
                .type(dto.getType())
                .build();

    }
}
