package application.utility;

public class Constants {
    public static final String DEFAULT = "default";
    public static final String INTEGRATION_TEST = "integration-test";
    public static final String DOCKER = "docker";
}
