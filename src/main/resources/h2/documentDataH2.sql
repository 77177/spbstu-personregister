insert into document (id, country, document_number, type, person_id, start_date, expired_date) values (1, 'PE', '0268;1516', 'PASSPORT', 1, '2019-10-10', '2019-05-15');
insert into document (id, country, document_number, type, person_id, start_date, expired_date) values (2, 'DO', '0009;0347', 'PASSPORT', 2, '2019-10-22', '2019-03-12');
insert into document (id, country, document_number, type, person_id, start_date, expired_date) values (3, 'CN', '57664;126', 'PASSPORT', 3, '2019-02-07', '2019-11-22');
insert into document (id, country, document_number, type, person_id, start_date, expired_date) values (4, 'PH', '43598;313', 'PASSPORT', 4, '2019-01-14', '2019-01-07');
