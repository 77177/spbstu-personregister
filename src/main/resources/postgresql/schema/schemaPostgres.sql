DROP TABLE IF EXISTS public.document;
DROP TABLE IF EXISTS public.person;
DROP SEQUENCE IF EXISTS public.person_id_seq;
DROP SEQUENCE IF EXISTS public.document_id_seq;

CREATE SEQUENCE if not exists public.person_id_seq;
CREATE SEQUENCE if not exists public.document_id_seq;

CREATE TABLE if not exists public.person
(
    birthday date,
    id bigint NOT NULL DEFAULT nextval('person_id_seq'::regclass),
    email character varying(255) COLLATE pg_catalog."default",
    f character varying(255) COLLATE pg_catalog."default",
    i character varying(255) COLLATE pg_catalog."default",
    o character varying(255) COLLATE pg_catalog."default",
    telephone character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT person_pkey PRIMARY KEY (id)
);

CREATE TABLE if not exists public.document
(
    id bigint NOT NULL DEFAULT nextval('document_id_seq'::regclass),
    country character varying(255) COLLATE pg_catalog."default",
    document_number character varying(255) COLLATE pg_catalog."default",
    expired_date date,
    start_date date,
    type character varying(255) COLLATE pg_catalog."default",
    person_id bigint,
    CONSTRAINT document_pkey PRIMARY KEY (id),
    FOREIGN KEY (person_id) REFERENCES person(id)
);

create extension if not exists pg_trgm;

create or replace function conv_f(in_fio text)
returns text language 'plpgsql' immutable as ' DECLARE
  BEGIN
    return regexp_replace(lower(in_fio), ''ё'', ''е'', ''g'');
    exception
    when others then raise exception ''%'', sqlerrm;
  END; ';

create or replace function conv_i(in_fio text)
returns text language 'plpgsql' immutable as ' DECLARE
  BEGIN
    return regexp_replace(lower(in_fio), ''ё'', ''е'', ''g'');
    exception
    when others then raise exception ''%'', sqlerrm;
  END; ';

create or replace function conv_o(in_fio text)
returns text language 'plpgsql' immutable as ' DECLARE
  BEGIN
    return regexp_replace(lower(in_fio), ''ё'', ''е'', ''g'');
    exception
    when others then raise exception ''%'', sqlerrm;
  END; ';

create or replace function conv_dn(in_fio text)
returns text language 'plpgsql' immutable as ' DECLARE
  BEGIN
    return regexp_replace(lower(in_fio), ''ё'', ''е'', ''g'');
    exception
    when others then raise exception ''%'', sqlerrm;
  END; ';

create index if not exists info_gist_idx_doc_1 on document
using gist(conv_dn(document_number) gist_trgm_ops);

create index if not exists info_gist_idx1 on person
using gist(conv_f(f) gist_trgm_ops);

create index if not exists info_gist_idx2 on person
using gist(conv_i(i) gist_trgm_ops);

create index if not exists info_gist_idx3 on person
using gist(conv_o(o) gist_trgm_ops);




