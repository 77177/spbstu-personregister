package application.utility;

import application.document.dtos.DocumentDto;
import application.document.entities.Document;
import application.person.dtos.PersonDto;
import application.person.entities.Person;
import application.person.services.PersonService;
import application.testInfrastructureClasses.statics.TestDataGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class ConverterTest {

    @Mock
    PersonService personService;

    @InjectMocks
    Converter converter;

    @Test
    public void testPersonDtoToPerson() {
        PersonDto testPersonDto = TestDataGenerator.getTestPersonDto();

        doReturn(TestDataGenerator.getTestPerson()).when(personService).get(testPersonDto.getId());

        Person personFromDto = converter.personDtoToPerson(testPersonDto);

        Person testPerson = TestDataGenerator.getTestPerson();

        assertEquals(testPerson.getBirthday(), personFromDto.getBirthday());
        assertEquals(testPerson.getDocuments(), personFromDto.getDocuments());
        assertEquals(testPerson.getEmail(), personFromDto.getEmail());
        assertEquals(testPerson.getTelephone(), personFromDto.getTelephone());
        assertEquals(testPerson.getF(), personFromDto.getF());
        assertEquals(testPerson.getI(), personFromDto.getI());
        assertEquals(testPerson.getO(), personFromDto.getO());
    }

    @Test
    public void testPersonDtoToPersonDocEmpty() {
        PersonDto testPersonDto = TestDataGenerator.getTestPersonDto();

        Person personFromDto = converter.personDtoToPersonDocEmpty(testPersonDto);

        Person testPerson = TestDataGenerator.getTestPerson();
        testPerson.setDocuments(Collections.emptyList());

        assertEquals(testPerson.getBirthday(), personFromDto.getBirthday());
        assertEquals(testPerson.getDocuments(), personFromDto.getDocuments());
        assertEquals(testPerson.getEmail(), personFromDto.getEmail());
        assertEquals(testPerson.getTelephone(), personFromDto.getTelephone());
        assertEquals(testPerson.getF(), personFromDto.getF());
        assertEquals(testPerson.getI(), personFromDto.getI());
        assertEquals(testPerson.getO(), personFromDto.getO());
    }

    @Test
    public void testDocumentDtoToDocument() {
        DocumentDto testDocumentDto = TestDataGenerator.getTestDocumentDto();

        doReturn(TestDataGenerator.getTestPerson()).when(personService).get(testDocumentDto.getPersonId());

        Document documentFromDto = converter.documentDtoToDocument(testDocumentDto);

        Document document = TestDataGenerator.getTestDocument();

        assertEquals(document.getCountry(), documentFromDto.getCountry());
        assertEquals(document.getDocumentNumber(), documentFromDto.getDocumentNumber());
        assertEquals(document.getStartDate(), documentFromDto.getStartDate());
        assertEquals(document.getExpiredDate(), documentFromDto.getExpiredDate());
        assertEquals(document.getPerson(), documentFromDto.getPerson());
        assertEquals(document.getType(), documentFromDto.getType());
    }
}