package application.testInfrastructureClasses.statics;

public class TestConstants {

    public static final String INTEGRATION_TEST = "integration-test";
    public static final String DEFAULT = "default";
    public static final String HTTP = "http";
    public static final String LOCALHOST = "localhost";

}
