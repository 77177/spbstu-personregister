package application.testInfrastructureClasses.statics;

import application.document.dtos.DocumentDto;
import application.document.entities.Document;
import application.person.dtos.PersonDto;
import application.person.entities.Person;

import java.time.LocalDate;
import java.util.Arrays;

public class TestDataGenerator {

    static public Person getTestPerson() {

        return Person.builder()
                .id(1L)
                .documents(Arrays.asList(new Document()))
                .f("1")
                .i("2")
                .o("3")
                .birthday(LocalDate.of(1111,11,11))
                .email("email")
                .telephone("+678")
                .build();
    }

    static public Document getTestDocument() {

        return Document.builder()
                .person(getTestPerson())
                .id(1L)
                .startDate(LocalDate.of(1000,11,11))
                .expiredDate(LocalDate.of(1111,11,11))
                .documentNumber("123;123")
                .country("RU")
                .type("PASSPORT")
                .build();
    }

    static public PersonDto getTestPersonDto() {

        return PersonDto.builder()
                .id(1L)
                .f("1")
                .i("2")
                .o("3")
                .birthday(LocalDate.of(1111,11,11))
                .email("email")
                .telephone("+678")
                .build();
    }

    static public DocumentDto getTestDocumentDto() {

        return DocumentDto.builder()
                .personId(1L)
                .id(1L)
                .startDate(LocalDate.of(1000,11,11))
                .expiredDate(LocalDate.of(1111,11,11))
                .documentNumber("123;123")
                .country("RU")
                .type("PASSPORT")
                .build();
    }
}
