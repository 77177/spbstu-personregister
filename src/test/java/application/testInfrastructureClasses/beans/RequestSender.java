package application.testInfrastructureClasses.beans;

import application.document.dtos.DocumentDto;
import application.document.entities.Document;
import application.document.repositories.DocumentRepository;
import application.person.dtos.PersonDto;
import application.person.entities.Person;
import application.person.repositories.PersonRepository;
import lombok.Setter;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static application.testInfrastructureClasses.statics.TestConstants.*;
import static application.testInfrastructureClasses.statics.TestDataGenerator.getTestDocumentDto;
import static application.testInfrastructureClasses.statics.TestDataGenerator.getTestPersonDto;
import static java.util.Objects.requireNonNull;

@Component
@Profile({INTEGRATION_TEST, DEFAULT})
@Setter
public class RequestSender {

    private Integer port;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    DocumentRepository documentRepository;

    public void deletePersonEntity(long id) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        URI deleteURI = new URI(HTTP + "://" + LOCALHOST + ":" + port + "/persons/delete/" + id);
        restTemplate.delete(deleteURI);
    }

    public void deleteDocumentEntity(long id) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        URI deleteURI = new URI(HTTP + "://" + LOCALHOST + ":" + port + "/documents/delete/" + id);
        restTemplate.delete(deleteURI);
    }

    public Person getPersonEntity(long id) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        URI getURI = new URI(HTTP + "://" + LOCALHOST + ":" + port + "/persons/get/" + id);
        ResponseEntity<Person> personEntity = restTemplate.getForEntity(getURI, Person.class);
        return personEntity.getBody();
    }

    public Document getDocumentEntity(long id) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        URI getURI = new URI(HTTP + "://" + LOCALHOST + ":" + port + "/documents/get/" + id);
        ResponseEntity<Document> personEntity = restTemplate.getForEntity(getURI, Document.class);
        return personEntity.getBody();
    }


    public List<Person> getAllPersons() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        URI getURI = new URI(HTTP + "://" + LOCALHOST + ":" + port + "/persons/get/all");
        Person[] forNow = restTemplate.getForObject(getURI, Person[].class);
        return Arrays.asList(requireNonNull(forNow));
    }

    public List<Person> findPerson(Person person) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        String email = person.getEmail();
        String f = person.getF();
        String i = person.getI();
        String o = person.getO();
        String telephone = person.getTelephone();
        LocalDate birthday = person.getBirthday();

        URIBuilder uriBuilder = new URIBuilder(HTTP + "://" + LOCALHOST + ":" + port + "/persons/findNoMetric");
        uriBuilder.addParameter("birthday", birthday.toString());
        uriBuilder.addParameter("email", email);
        uriBuilder.addParameter("f", f);
        uriBuilder.addParameter("i", i);
        uriBuilder.addParameter("o", o);
        uriBuilder.addParameter("telephone", telephone);
        String url = uriBuilder.toString();

        URI getURI = new URI(url);
        Person[] forNow = restTemplate.getForObject(getURI, Person[].class);
        return Arrays.asList(requireNonNull(forNow));
    }

    public Long postDocumentEntity(long id, long personId) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        DocumentDto testDocumentDto = getTestDocumentDto();
        testDocumentDto.setId(id);
        testDocumentDto.setPersonId(personId);

        URI createURI = new URI(HTTP + "://" + LOCALHOST + ":" + port + "/documents/post");
        HttpEntity<DocumentDto> httpEntity = new HttpEntity<>(testDocumentDto, httpHeaders);
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(createURI, httpEntity, String.class);

        return Long.parseLong(requireNonNull(stringResponseEntity.getBody()));
    }

    public Long postPersonEntity(long id) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        PersonDto testPersonDto = getTestPersonDto();
        testPersonDto.setId(id);

        URI createURI = new URI(HTTP + "://" + LOCALHOST + ":" + port + "/persons/post");
        HttpEntity<PersonDto> httpEntity = new HttpEntity<>(testPersonDto, httpHeaders);
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(createURI, httpEntity, String.class);

        return Long.parseLong(requireNonNull(stringResponseEntity.getBody()));
    }


    public void putPersonEntity(long id) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        PersonDto testPersonDto = getTestPersonDto();
        testPersonDto.setId(id);

        URI createURI = new URI(HTTP + "://" + LOCALHOST + ":" + port + "/persons/put");
        HttpEntity<PersonDto> httpEntity = new HttpEntity<>(testPersonDto, httpHeaders);
        restTemplate.put(createURI, httpEntity);
    }

    public void putDocumentEntity(long id, long personId) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        DocumentDto testDocumentDto = getTestDocumentDto();
        testDocumentDto.setId(id);
        testDocumentDto.setPersonId(personId);

        URI createURI = new URI(HTTP + "://" + LOCALHOST + ":" + port + "/documents/put");
        HttpEntity<DocumentDto> httpEntity = new HttpEntity<>(testDocumentDto, httpHeaders);
        restTemplate.put(createURI, httpEntity);

    }

    public long getPersonTableSize() {
        return personRepository.count();
    }

    public long getDocumentTableSize() {
        return documentRepository.count();
    }

    public List<Person> findPersonByDocNumber(String documentNumber) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        URIBuilder uriBuilder = new URIBuilder(HTTP + "://" + LOCALHOST + ":" + port + "/persons/findByDocNoMetric");
        uriBuilder.addParameter("documentNumber", documentNumber);
        String url = uriBuilder.toString();

        URI getURI = new URI(url);
        Person[] forNow = restTemplate.getForObject(getURI, Person[].class);
        return Arrays.asList(requireNonNull(forNow));
    }
}
