package application;


import application.document.services.DocumentService;
import application.testInfrastructureClasses.beans.RequestSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URISyntaxException;
import java.util.Random;

import static application.testInfrastructureClasses.statics.TestConstants.INTEGRATION_TEST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@RunWith(SpringRunner.class)
@ActiveProfiles(INTEGRATION_TEST)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = Main.class)
public class MainCRUDAPIDocumentTest {

    @LocalServerPort
    Integer port;

    @Autowired
    RequestSender requestSender;

    @Autowired
    DocumentService documentService;

    @Test
    public void testCreateDocument() throws URISyntaxException {
        requestSender.setPort(port);

        long personId = -1;

        long randomId = new Random().nextLong();
        for (int i = 0; i < 3; i++) {
            personId = requestSender.postPersonEntity(i);
        }

        long documentTableSizeBefore = requestSender.getDocumentTableSize();
        requestSender.postDocumentEntity(randomId, personId);
        long documentTableSizeAfter = requestSender.getDocumentTableSize();

        assertThat(documentTableSizeAfter - documentTableSizeBefore).isEqualTo(1);

        documentService.deleteAllDocuments();
    }

    @Test
    public void testUpdateDocument() throws URISyntaxException {
        requestSender.setPort(port);

        long personId = 0;
        long randomId = new Random().nextLong();
        for (int i = 0; i < 3; i++) {
            personId = requestSender.postPersonEntity(i);
        }

        Long returnedId = requestSender.postDocumentEntity(randomId, personId);

        long documentTableSizeBefore = requestSender.getDocumentTableSize();
        requestSender.putDocumentEntity(returnedId, personId);
        long documentTableSizeAfter = requestSender.getDocumentTableSize();

        assertThat(documentTableSizeAfter - documentTableSizeBefore).isEqualTo(0);
        documentService.deleteAllDocuments();
    }

    @Test
    public void testDeleteDocumentSuccess() throws URISyntaxException {
        requestSender.setPort(port);

        long personId = 0;
        for (int i = 0; i < 3; i++) {
            personId = requestSender.postPersonEntity(i);
        }

        long randomId = new Random().nextLong();
        Long returnedId = requestSender.postDocumentEntity(randomId, personId);

        long documentTableSizeBefore = requestSender.getDocumentTableSize();
        requestSender.deleteDocumentEntity(returnedId);
        long documentTableSizeAfter = requestSender.getDocumentTableSize();

        assertThat(documentTableSizeAfter - documentTableSizeBefore).isEqualTo(-1);
        documentService.deleteAllDocuments();
    }

    @Test
    public void testDeleteDocumentFailure() {
        long nonExistentId = 100;

        Throwable throwable = catchThrowable(() -> requestSender.deleteDocumentEntity(nonExistentId));

        assertThat(throwable).isInstanceOf(Exception.class);
    }

    @Test
    public void testGetDocumentSuccess() throws URISyntaxException {
        requestSender.setPort(port);

        long personId = 0;
        for (int i = 0; i < 3; i++) {
            personId = requestSender.postPersonEntity(i);
        }

        long randomId = new Random().nextLong();
        Long returnedId = requestSender.postDocumentEntity(randomId, personId);

        Throwable throwable = catchThrowable(() -> requestSender.getDocumentEntity(returnedId));

        assertThat(throwable).isEqualTo(null);

        documentService.deleteAllDocuments();
    }

    @Test
    public void testGetDocumentFailure() {
        requestSender.setPort(port);

        long nonExistentId = 200;

        Throwable throwable = catchThrowable(() -> requestSender.getDocumentEntity(nonExistentId));

        assertThat(throwable).isInstanceOf(Exception.class);
    }
}