package application.document.entities;

import application.testInfrastructureClasses.statics.TestDataGenerator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DocumentTest {

    @Test
    public void testApply() {
        // Setup
        final Document document = TestDataGenerator.getTestDocument();

        Document documentUnderTest = new Document();
        // Run the test
        documentUnderTest.apply(document);

        // Verify the results
        assertEquals(document.getCountry(), documentUnderTest.getCountry());
        assertEquals(document.getDocumentNumber(), documentUnderTest.getDocumentNumber());
        assertEquals(document.getStartDate(), documentUnderTest.getStartDate());
        assertEquals(document.getExpiredDate(), documentUnderTest.getExpiredDate());
        assertEquals(document.getPerson(), documentUnderTest.getPerson());
        assertEquals(document.getType(), documentUnderTest.getType());
    }
}
