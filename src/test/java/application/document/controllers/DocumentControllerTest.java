package application.document.controllers;

import application.document.dtos.DocumentDto;
import application.document.entities.Document;
import application.document.services.DocumentService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;


public class DocumentControllerTest {

    @Mock
    private DocumentService mockDocumentService;

    private DocumentController documentControllerUnderTest;

    @Before
    public void setUp() {
        initMocks(this);
        documentControllerUnderTest = new DocumentController(mockDocumentService);
    }

    @Test
    public void testGet() {
        // Setup
        Document mock = mock(Document.class);

        when(mockDocumentService.get(0L)).thenReturn(mock);

        // Run the test
        Document result = documentControllerUnderTest.get(0L);

        // Verify the results
        assertEquals(result, mock);
        verify(mockDocumentService).get(0);
    }

    @Test
    public void testPost() {
        // Setup
        final DocumentDto dto = mock(DocumentDto.class);

        when(mockDocumentService.createOrUpdate(any(DocumentDto.class))).thenReturn(0L);

        // Run the test
        final Long result = documentControllerUnderTest.post(dto);

        // Verify the results
        assertEquals(new Long(0L), result);
        verify(mockDocumentService).createOrUpdate(any(DocumentDto.class));
    }

    @Test
    public void testPut() {
        // Setup
        final DocumentDto dto = mock(DocumentDto.class);

        when(mockDocumentService.createOrUpdate(any(DocumentDto.class))).thenReturn(0L);

        // Run the test
        final Long result = documentControllerUnderTest.put(dto);

        // Verify the results
        assertEquals(new Long(0L), result);
        verify(mockDocumentService).createOrUpdate(any(DocumentDto.class));
    }

    @Test
    public void testDelete() {
        // Setup

        // Run the test
        documentControllerUnderTest.delete(0L);

        // Verify the results
        verify(mockDocumentService).delete(0L);
    }
}
