package application.document.services;

import application.document.dtos.DocumentDto;
import application.document.entities.Document;
import application.document.repositories.DocumentRepository;
import application.person.entities.Person;
import application.testInfrastructureClasses.statics.TestDataGenerator;
import application.utility.Converter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.data.util.Pair;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class DocumentServiceTest {

    @Mock
    private DocumentRepository mockDocumentRepository;

    private DocumentService documentServiceUnderTest;

    @Mock
    private Converter converter;

    @Before
    public void setUp() {
        initMocks(this);
        documentServiceUnderTest = spy(new DocumentService(mockDocumentRepository, converter));
    }

    @Test
    public void testGet() {
        Optional<Document> document = Optional.of(TestDataGenerator.getTestDocument());

        // Setup
        when(mockDocumentRepository.findById(0L)).thenReturn(document);

        // Run the test
        final Document result = documentServiceUnderTest.get(0L);

        // Verify the results
        verify(mockDocumentRepository).findById(0L);
        assertEquals(document.get(), result);
    }

    @Test
    public void testFindPersonByDocNumber() {
        // Setup
        final List<Pair<Double, Person>> expectedResult = Arrays.asList(Pair.of(0.0, TestDataGenerator.getTestPerson()));
        // Configure DocumentRepository.fuzzyFind(...).
        final List<Document> documents = Arrays.asList(TestDataGenerator.getTestDocument());

        String documentNumber = "documentNumber";

        when(mockDocumentRepository.fuzzyFind(documentNumber)).thenReturn(documents);

        when(mockDocumentRepository.getMetric(documentNumber)).thenReturn(Arrays.asList(0.0));

        // Run the test
        final List<Pair<Double, Person>> result = documentServiceUnderTest.findPersonByDocNumber(documentNumber);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testFindPersonByDocNumberNoMetric() {
        // Setup
        final List<Person> expectedResult = Arrays.asList(TestDataGenerator.getTestPerson());

        // Configure DocumentRepository.fuzzyFind(...).
        final List<Document> documents = Arrays.asList(TestDataGenerator.getTestDocument());

        String documentNumber = "documentNumber";
        when(mockDocumentRepository.fuzzyFind(documentNumber)).thenReturn(documents);

        // Run the test
        final List<Person> result = documentServiceUnderTest.findPersonByDocNumberNoMetric(documentNumber);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testCreateOrUpdate1() {
        // Setup
        long documentId = 0L;

        final DocumentDto dto = mock(DocumentDto.class);

        final Document document = mock(Document.class);
        when(document.getId()).thenReturn(documentId);

        final Document documentFromRepo = mock(Document.class);
        when(document.getId()).thenReturn(documentId);

        doReturn(true).when(documentServiceUnderTest).hasProperInfo(any());
        when(converter.documentDtoToDocument(dto)).thenReturn(document);
        when(mockDocumentRepository.existsById(documentId)).thenReturn(true);
        when(mockDocumentRepository.getOne(documentId)).thenReturn(documentFromRepo);

        // Run the test
        final Long result = documentServiceUnderTest.createOrUpdate(dto);

        // Verify the results
        verify(mockDocumentRepository).getOne(documentId);
        verify(documentFromRepo).apply(document);
        assertEquals(new Long(documentId), result);
    }

    @Test
    public void testCreateOrUpdate2() {
        // Setup
        long documentId = 0L;

        final DocumentDto dto = mock(DocumentDto.class);

        final Document document = mock(Document.class);
        when(document.getId()).thenReturn(documentId);

        doReturn(true).when(documentServiceUnderTest).hasProperInfo(any());
        final Document documentFromRepo = mock(Document.class);
        when(document.getId()).thenReturn(documentId);

        when(converter.documentDtoToDocument(dto)).thenReturn(document);
        when(mockDocumentRepository.existsById(documentId)).thenReturn(false);
        when(mockDocumentRepository.save(document)).thenReturn(document);

        // Run the test
        final Long result = documentServiceUnderTest.createOrUpdate(dto);

        // Verify the results
        verify(mockDocumentRepository).save(document);
        assertEquals(new Long(documentId), result);
    }

    @Test
    public void testDelete() {
        // Setup

        // Run the test
        documentServiceUnderTest.delete(0L);

        // Verify the results
        verify(mockDocumentRepository).deleteById(0L);
    }

    @Test
    public void testDeleteAllDocuments() {
        // Setup

        // Run the test
        documentServiceUnderTest.deleteAllDocuments();

        // Verify the results
        verify(mockDocumentRepository).deleteAll();
    }

    @Test
    public void testGetAllDocuments() {
        // Setup
        final List<Document> expectedResult = Arrays.asList(TestDataGenerator.getTestDocument());

        // Configure DocumentRepository.findAll(...).
        final List<Document> documents = Arrays.asList(TestDataGenerator.getTestDocument());
        when(mockDocumentRepository.findAll()).thenReturn(documents);

        // Run the test
        final List<Document> result = documentServiceUnderTest.getAllDocuments();

        // Verify the results
        assertEquals(expectedResult, result);
    }
}
