package application;


import application.document.entities.Document;
import application.person.entities.Person;
import application.person.services.PersonService;
import application.testInfrastructureClasses.beans.RequestSender;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Random;

import static application.testInfrastructureClasses.statics.TestConstants.DEFAULT;
import static java.util.stream.Collectors.toList;

@RunWith(SpringRunner.class)
@ActiveProfiles(DEFAULT)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = Main.class)
public class MainFuzzySearchTest {

    @LocalServerPort
    Integer port;

    @Autowired
    RequestSender requestSender;

    @Autowired
    PersonService personService;

    private String changeString(String s) {
        Random random = new Random();
        char[] charactersOfAString = s.toCharArray();
        int randomIndex = random.nextInt(charactersOfAString.length);
        char character = (char) (random.nextInt(94) + 32);
        charactersOfAString[randomIndex] = character;
        return new String(charactersOfAString);
    }

    private Person randomize(Person person) {

        String email = person.getEmail();
        String noisyEmail = changeString(email);
        person.setEmail(noisyEmail);

        String f = person.getF();
        String noisyF = changeString(f);
        person.setF(noisyF);

        String i = person.getI();
        String noisyI = changeString(i);
        person.setI(noisyI);

        String o = person.getO();
        String noisyO = changeString(o);
        person.setO(noisyO);

        String telephone = person.getTelephone();
        String noisyTelephone = changeString(telephone);
        person.setTelephone(noisyTelephone);

        return person;
    }


    @Test
    @Ignore
    public void testFindPerson() throws URISyntaxException {
        requestSender.setPort(port);

        Long personEntityId = requestSender.postPersonEntity(1);

        List<Person> allPersons = requestSender.getAllPersons();

        List<Person> randomizedPersons = allPersons.stream()
                .map(this::randomize)
                .collect(toList());

        List<Person> hundredRandomizedPersons = randomizedPersons.stream()
          .limit(1)
          .collect(toList());

        double positiveMatch = 0;
        double negativeMatch = 0;

        for (Person randomizedPerson : hundredRandomizedPersons) {
            List<Person> personList = requestSender.findPerson(randomizedPerson);

            List<Long> ids = personList.stream().map(Person::getId).collect(toList());

            if (ids.contains(randomizedPerson.getId())) {
                ++positiveMatch;
            } else {
                ++negativeMatch;
            }
        }

        double percentageOfPositiveMatches = (positiveMatch / (negativeMatch + positiveMatch)) * 100;

        System.out.println(percentageOfPositiveMatches);

        requestSender.deletePersonEntity(personEntityId);

        Assert.assertTrue(percentageOfPositiveMatches >= 70);
    }


    @Test
    @Ignore
    public void testFindPersonByDocNumber() throws URISyntaxException {
        requestSender.setPort(port);

        Long personEntityId = requestSender.postPersonEntity(1);
        requestSender.postDocumentEntity(1,personEntityId);

        List<Person> allPersons = requestSender.getAllPersons();

        List<Person> hundredPersons = allPersons.stream()
                .limit(1)
                .collect(toList());

        double positiveMatch = 0;
        double negativeMatch = 0;

        for (Person randomizedPerson : hundredPersons) {

            Document document = randomizedPerson.getDocuments().get(0);

            document.setDocumentNumber(changeString(document.getDocumentNumber()));

            List<Person> personList = requestSender.findPersonByDocNumber(document.getDocumentNumber());

            List<Long> ids = personList.stream().map(Person::getId).collect(toList());

            if (ids.contains(randomizedPerson.getId())) {
                ++positiveMatch;
            } else {
                ++negativeMatch;
            }
        }

        double percentageOfPositiveMatches = (positiveMatch / (negativeMatch + positiveMatch)) * 100;

        System.out.println(percentageOfPositiveMatches);

        requestSender.deletePersonEntity(personEntityId);

        Assert.assertTrue(percentageOfPositiveMatches >= 70);
    }
}