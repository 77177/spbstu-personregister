package application.person.services;

import application.document.services.DocumentService;
import application.person.dtos.PersonDto;
import application.person.entities.Person;
import application.person.repositories.PersonRepository;
import application.testInfrastructureClasses.statics.TestDataGenerator;
import application.utility.Converter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.util.Pair;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {

  @InjectMocks
  PersonService personService;

  @Mock
  PersonRepository personRepository;

  @Mock
  DocumentService documentService;

  @Mock
  Converter converter;

  @Test
  public void testGetPersonSuccess() {
    Person testPerson = mock(Person.class);
    long id = 1;

    doReturn(Optional.ofNullable(testPerson)).when(personRepository).findById(id);

    Person personFromRepo = personService.get(id);

    assertEquals(personFromRepo, testPerson);

  }

  @Test(expected = Exception.class)
  public void testGetPersonFailure() {
    long id = 1;

    doReturn(Optional.empty()).when(personRepository).findById(id);

    personService.get(id);
  }

  @Test
  public void testCreateOrUpdatePerson1() {
    PersonDto testPersonDto = mock(PersonDto.class);
    Person testPerson = mock(Person.class);
    Person returnedTestPerson = mock(Person.class);

    doReturn(testPerson).when(converter).personDtoToPersonDocEmpty(testPersonDto);
    doReturn(false).when(personRepository).existsById(any());
    doReturn(returnedTestPerson).when(personRepository).save(testPerson);
    doReturn(0L).when(returnedTestPerson).getId();

    personService.createOrUpdate(testPersonDto);

    verify(personRepository).save(testPerson);
    verify(personRepository).existsById(anyLong());
    verify(converter).personDtoToPersonDocEmpty(testPersonDto);
    verifyNoMoreInteractions(converter);
    verifyNoMoreInteractions(personRepository);
  }


  @Test
  public void testCreateOrUpdatePerson2() {
    PersonDto testPersonDto = mock(PersonDto.class);
    Person testPerson = mock(Person.class);

    doReturn(testPerson).when(converter).personDtoToPerson(testPersonDto);
    doReturn(true).when(personRepository).existsById(any());
    doReturn(testPerson).when(personRepository).getOne(any());

    personService.createOrUpdate(testPersonDto);

    verify(personRepository).getOne(any());
    verify(personRepository).existsById(anyLong());
    verify(converter).personDtoToPerson(testPersonDto);
    verifyNoMoreInteractions(converter);
    verifyNoMoreInteractions(personRepository);
  }

  @Test
  public void testDeletePerson() {
    long id = 1;

    personService.delete(id);

    verify(personRepository).deleteById(id);
    verifyNoMoreInteractions(personRepository);
  }

  @Test
  public void testFindPerson() {

    Person person = mock(Person.class);

    final List<Person> expectedResult = Arrays.asList(person);
    double metric = 0.0D;

    final List<Double> expectedMetric = Arrays.asList(metric);
    // Configure PersonRepository.fuzzyFind(...).
    final List<Person> people = Arrays.asList(person);
    when(personRepository
            .fuzzyFind(
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString(),
                    any(),
                    anyString()
            ))
            .thenReturn(people);

    when(personRepository
            .getMetric(
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString(),
                    any(),
                    anyString()
            ))
            .thenReturn(expectedMetric);

    // Run the test
    final List<Pair<Double,Person>> result = personService
            .findPerson(
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString(),
                    any());

    // Verify the results
    assertEquals(Arrays.asList(Pair.of(expectedMetric.get(0), expectedResult.get(0))),result);
    verify(personRepository).fuzzyFind(
            anyString(),
            anyString(),
            anyString(),
            anyString(),
            any(),
            anyString());
    verify(personRepository).getMetric(
            anyString(),
            anyString(),
            anyString(),
            anyString(),
            any(),
            anyString());

  }

  @Test
  public void testFindPersonNoMetric() {
    // Setup
    Person person = mock(Person.class);

    final List<Person> expectedResult = Arrays.asList(person);
    // Configure PersonRepository.fuzzyFind(...).
    final List<Person> people = Arrays.asList(person);
    when(personRepository
            .fuzzyFind(
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString(),
                    any(),
                    anyString()
                    ))
            .thenReturn(people);

    // Run the test
    final List<Person> result = personService
            .findPersonNoMetric(
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString(),
                    any());

    // Verify the results
    assertEquals(expectedResult, result);
  }

  @Test
  public void testGetAllPersons() {
    // Setup
    Person mockPerson = mock(Person.class);
    final List<Person> expectedResult = Collections.singletonList(mockPerson);
    when(personRepository.findAll()).thenReturn(Collections.singletonList(mockPerson));

    // Run the test
    final List<Person> result = personService.getAllPersons();

    // Verify the results
    assertEquals(expectedResult, result);
  }


  @Test
  public void testFindPersonByDocumentNumber() {

    String documentNumber = "documentNumber";

    // Setup
    final List<Pair<Double, Person>> expectedResult = Arrays.asList(Pair.of(0.0,TestDataGenerator.getTestPerson()));

    // Run the test
    final List<Pair<Double, Person>> returnResult = Arrays.asList(Pair.of(0.0,TestDataGenerator.getTestPerson()));

    doReturn(returnResult).when(documentService).findPersonByDocNumber(documentNumber);

    // Run the test
    final List<Pair<Double, Person>> result = personService.findPersonByDocumentNumber(documentNumber);

    // Verify the results
    assertEquals(expectedResult, result);
  }

  @Test
  public void testFindPersonByDocumentNumberNoMetric() {

    String documentNumber = "documentNumber";

    // Setup
    final List<Person> expectedResult = Arrays.asList(TestDataGenerator.getTestPerson());
    final List<Person> returnResult = Arrays.asList(TestDataGenerator.getTestPerson());

    doReturn(returnResult).when(documentService).findPersonByDocNumberNoMetric(documentNumber);

    // Run the test
    final List<Person> result = personService.findPersonByDocumentNumberNoMetric(documentNumber);

    // Verify the results
    assertEquals(expectedResult, result);
  }


}