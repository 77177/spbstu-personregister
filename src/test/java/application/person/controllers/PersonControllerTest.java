package application.person.controllers;

import application.person.dtos.PersonDto;
import application.person.entities.Person;
import application.person.services.PersonService;
import application.testInfrastructureClasses.statics.TestDataGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.util.Pair;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PersonControllerTest {

    @InjectMocks
    PersonController personController;

    @Mock
    PersonService personService;

    @Test
    public void testGetPerson() {
        Person testPerson = mock(Person.class);
        long id = 1;

        doReturn(testPerson).when(personService).get(id);

        Person person = personController.get(id);

        verify(personService).get(id);
        assertEquals(person, testPerson);
    }

    @Test
    public void testGetAllPersons() {
        Person person = mock(Person.class);

        // Setup
        final List<Person> expectedResult = Arrays.asList(person);
        when(personService.getAllPersons()).thenReturn(Arrays.asList(person));

        // Run the test
        final List<Person> result = personController.getAllPersons();

        // Verify the results
        verify(personService).getAllPersons();
        assertEquals(expectedResult, result);
    }

    @Test
    public void testPutPerson() {
        // Setup
        final PersonDto personDto = mock(PersonDto.class);

        final Long expectedResult = 1L;

        when(personService.createOrUpdate(personDto)).thenReturn(expectedResult);

        // Run the test
        final Long result = personController.put(personDto);

        // Verify the results
        assertEquals(expectedResult, result);
        verify(personService).createOrUpdate(personDto);
    }

    @Test
    public void testPostPerson() {
        PersonDto personDto = mock(PersonDto.class);

        personController.post(personDto);

        verify(personService).createOrUpdate(personDto);
        verifyNoMoreInteractions(personService);
    }

    @Test
    public void testDeletePerson() {
        long id = 1;

        personController.delete(id);

        verify(personService).delete(id);
        verifyNoMoreInteractions(personService);
    }

    @Test
    public void testFindPersonByDocumentNumber() {

        // Setup
        final List<Pair<Double, Person>> expectedResult = Arrays.asList(Pair.of(0.0, TestDataGenerator.getTestPerson()));

        // Configure PersonService.findPersonByDocumentNumber(...).
        final List<Pair<Double, Person>> pairs = Arrays.asList(Pair.of(0.0,TestDataGenerator.getTestPerson()));

        String documentNumber = "documentNumber";
        when(personService.findPersonByDocumentNumber(documentNumber)).thenReturn(pairs);

        // Run the test
        final List<Pair<Double, Person>> result = personController.findPersonByDocumentNumber(documentNumber);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testFindPersonByDocumentNumberNoMetric() {
        // Setup
        final List<Person> expectedResult = Arrays.asList(TestDataGenerator.getTestPerson());
        // Configure PersonService.findPersonByDocumentNumberNoMetric(...).
        final List<Person> people = Arrays.asList(TestDataGenerator.getTestPerson());

        String documentNumber = "documentNumber";
        when(personService.findPersonByDocumentNumberNoMetric(documentNumber)).thenReturn(people);

        // Run the test
        final List<Person> result = personController.findPersonByDocumentNumberNoMetric(documentNumber);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testFindPerson() {
        // Setup
        final List<Pair<Double, Person>> expectedResult = mock(List.class);

        // Configure PersonService.findPerson(...).
        when(personService.findPerson(any(), any(), any(), any(), any(), any())).thenReturn(expectedResult);

        // Run the test
        final List<Pair<Double, Person>> result = personController.findPerson( "email", "f", "i", "o",  LocalDate.of(2017, 1, 1), "telephone");

        // Verify the results
        assertEquals(expectedResult, result);
        verify(personService).findPerson(any(), any(), any(), any(), any(), any());
    }

    @Test
    public void testFindPersonNoMetric() {

        final List<Person> expectedResult = mock(List.class);

        // Configure PersonService.findPersonNoMetric(...).
        when(personService.findPersonNoMetric(any(), any(), any(), any(), any(), any())).thenReturn(expectedResult);

        // Run the test
        final List<Person> result = personController.findPersonNoMetric( "email", "f", "i", "o",  LocalDate.of(2017, 1, 1), "telephone");

        // Verify the results
        assertEquals(expectedResult, result);
        verify(personService).findPersonNoMetric(any(), any(), any(), any(), any(), any());

    }
}