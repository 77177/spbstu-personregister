package application.person.entities;

import application.testInfrastructureClasses.statics.TestDataGenerator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersonTest {

    @Test
    public void testApply() {
        // Setup
        final Person person = TestDataGenerator.getTestPerson();

        Person personUnderTest = new Person();

        // Run the test
        personUnderTest.apply(person);

        // Verify the results
         assertEquals(person.getF(),personUnderTest.getF());
         assertEquals(person.getI(),personUnderTest.getI());
         assertEquals(person.getO(),personUnderTest.getO());
         assertEquals(person.getEmail(),personUnderTest.getEmail());
         assertEquals(person.getTelephone(),personUnderTest.getTelephone());
         assertEquals(person.getBirthday(),personUnderTest.getBirthday());
         assertEquals(person.getDocuments(),personUnderTest.getDocuments());
    }
}
