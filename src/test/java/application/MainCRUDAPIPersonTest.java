package application;


import application.person.services.PersonService;
import application.testInfrastructureClasses.beans.RequestSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URISyntaxException;
import java.util.Random;

import static application.testInfrastructureClasses.statics.TestConstants.INTEGRATION_TEST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@RunWith(SpringRunner.class)
@ActiveProfiles(INTEGRATION_TEST)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = Main.class)
public class MainCRUDAPIPersonTest {

    @LocalServerPort
    Integer port;

    @Autowired
    RequestSender requestSender;

    @Autowired
    PersonService personService;

    @Test
    public void testCreatePerson() throws URISyntaxException {
        requestSender.setPort(port);

        long randomId = new Random().nextLong();

        long personTableSizeBefore = requestSender.getPersonTableSize();
        requestSender.postPersonEntity(randomId);
        long personTableSizeAfter = requestSender.getPersonTableSize();

        assertThat(personTableSizeAfter - personTableSizeBefore).isEqualTo(1);

        personService.deleteAllPersons();
    }

    @Test
    public void testUpdatePerson() throws URISyntaxException {
        requestSender.setPort(port);

        long randomId = new Random().nextLong();
        Long returnedId = requestSender.postPersonEntity(randomId);

        long personTableSizeBefore = requestSender.getPersonTableSize();
        requestSender.putPersonEntity(returnedId);
        long personTableSizeAfter = requestSender.getPersonTableSize();

        assertThat(personTableSizeAfter - personTableSizeBefore).isEqualTo(0);
        personService.deleteAllPersons();
    }

    @Test
    public void testDeletePersonSuccess() throws URISyntaxException {
        requestSender.setPort(port);

        long randomId = new Random().nextLong();
        Long returnedId = requestSender.postPersonEntity(randomId);

        long personTableSizeBefore = requestSender.getPersonTableSize();
        requestSender.deletePersonEntity(returnedId);
        long personTableSizeAfter = requestSender.getPersonTableSize();

        assertThat(personTableSizeAfter - personTableSizeBefore).isEqualTo(-1);
        personService.deleteAllPersons();
    }

    @Test
    public void testDeletePersonFailure() {
        long nonExistentId = 100;

        Throwable throwable = catchThrowable(() -> requestSender.deletePersonEntity(nonExistentId));

        assertThat(throwable).isInstanceOf(Exception.class);
    }

    @Test
    public void testGetPersonSuccess() throws URISyntaxException {
        requestSender.setPort(port);

        long randomId = new Random().nextLong();
        Long returnedId = requestSender.postPersonEntity(randomId);

        Throwable throwable = catchThrowable(() -> requestSender.getPersonEntity(returnedId));

        assertThat(throwable).isEqualTo(null);

        personService.deleteAllPersons();
    }

    @Test
    public void testGetPersonFailure() {
        requestSender.setPort(port);

        long nonExistentId = 200;

        Throwable throwable = catchThrowable(() -> requestSender.getPersonEntity(nonExistentId));

        assertThat(throwable).isInstanceOf(Exception.class);
    }
}